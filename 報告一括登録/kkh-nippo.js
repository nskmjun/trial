/**
 * @fileoverview 一括報告アプリ
 * - 営業活動レポートの一括登録
 *   日報登録アプリより *
 * 【必要ライブラリ】
 * [JavaScript]
 * jquery.min.js v2.2.3
 * snc.min.js
 * snc.kintone.min.js

 * @KKH_version 1.0.0
*/
jQuery.noConflict();
(function ($, config, configEigyoRepotoLinkage, sncLib) {
	'use strict';
	const cfgNippo = config.nippo;
	const cfgNippoFields = config.nippo.fields;
	const cfgNippoMessages = config.nippo.messages;

	/**
	 * 営業活動レポートへ登録する情報を作成
	 * @param {Object} record
	 * @param {Object} table
	 * @param {Object} postFields
	 */
	function createEigyoRepotoData(record, table, postFields) {
		// ヘッダー部の設定
		var headerData = postFields.header;
		// 明細部の設定
		var detailData = postFields.detail;
		// 登録対象情報を格納
		var postRecords = Array();
		// サブテーブルの行数
		var rowCount = table.length;
		// 登録用データの作成
		for (var i = 0; i < rowCount; i++) {
			var isBlankRow = true;
			var postRecord = {};
			// ヘッダー部
			for (var j = 0; j < headerData.length; j++) {
				var sCode = headerData[j].source.code;
				var tType = headerData[j].target.type;
				var tCode = headerData[j].target.code;
				var tValue = record[sCode].value;
				postRecord[tCode] = sncLib.kintone.util.setKintoneRecordValue(tType, tCode, tValue);
				//console.log(postRecord[code]);
			}
			// 明細部
			for (var j = 0; j < detailData.length; j++) {
				var sCode = detailData[j].source.code;
				var tType = detailData[j].target.type;
				var tCode = detailData[j].target.code;
				var tValue = table[i].value[sCode].value;
				if (tValue) {
					isBlankRow = false;
					//if($.isArray(tCode)) {
					if (sncLib.util.isArray(tCode)) {
						postRecord[tCode[0]] = sncLib.kintone.util.setKintoneRecordValue(tType, tCode[1], tValue);
					} else {
						postRecord[tCode] = sncLib.kintone.util.setKintoneRecordValue(tType, tCode, tValue);
					}
				}
			}

			// ブランク行ではない場合
			if (!isBlankRow) {
				// 登録用の情報を格納
				postRecords.push(postRecord);
			}
		}
		return postRecords;
	};

	/**
	 * 必須入力チェック
	 * 　各フィールドの必須入力チェック設定は、config側で指定
	 * @param {Object} event
	 * @param {Object} postFields
	 * @return {boolean}
	*/
	function isErrorRequiredCheck(event, postFields) {
		var record = event.record;
		var table = record[cfgNippoFields.eigyoRepoto.code].value;
		var headerData = postFields.header;
		var detailData = postFields.detail;
		var isError = false;
		var rowCount = table.length;

		// ヘッダー部
		for (var i = 0; i < headerData.length; i++) {
			var code = headerData[i].source.code;
			var value = record[code].value;
			// 必須項目、かつ、値がセットされていない場合
			if (headerData[i].source.required && !value) {
				isError = true;
				record[code].error = cfgNippoMessages.emptyError;
			}
		}

		// 明細部の行
		for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
			var isBlankRow = true;

			// ブランク行かをチェック
			for (var colIndex = 0; colIndex < detailData.length; colIndex++) {
				var code = detailData[colIndex].source.code;
				var value = table[rowIndex].value[code].value;
				// 値が配列の場合
				if (sncLib.util.isArray(value)) {
					if (value.length > 0) {
						isBlankRow = false;
						break;
					}
				}
				else {
					if (value) {
						isBlankRow = false;
						break;
					}
				}
			}

			// 明細部のカラム
			for (var colIndex = 0; colIndex < detailData.length; colIndex++) {
				var code = detailData[colIndex].source.code;
				var value = table[rowIndex].value[code].value;

				// ブランク行ではない、かつ
				// 必須項目と設定されている、かつ、値が入力されていない場合、エラーとする
				if (!isBlankRow
					&& detailData[colIndex].source.required && !value) {
					isError = true;
					table[rowIndex].value[code].error = cfgNippoMessages.emptyError;
				}
			}
		}
		return isError;
	}

	/**
	 * 入力値チェック
	 * 　各フィールドの必須入力チェック設定は、config側で指定
	 * @param {Object} record
	 * @return {boolean}
	*/
	function isErrorInputValue(record) {
		var table = record[cfgNippoFields.eigyoRepoto.code].value;
		var isError = false;
		var rowCount = table.length;
		var isError = false;

		// 明細部の行
		for (var rowIndex = 0; rowIndex < rowCount; rowIndex++) {
			// 活動日
			var katsudobi = record[cfgNippoFields.katsudobi.code].value;
			// 開始時刻
			var kaishiJikoku = table[rowIndex].value[cfgNippoFields.kaishijikoku.code].value;
			// 終了日
			var shuryobi = table[rowIndex].value[cfgNippoFields.shuryobi.code].value;
			// 終了時刻
			var shuryoJikoku = table[rowIndex].value[cfgNippoFields.shuryojikoku.code].value;

			if (katsudobi && shuryobi) {
				var diffDate = sncLib.util.calcDiffDate(sncLib.util.toDate(katsudobi), sncLib.util.toDate(shuryobi));
				// 日付比較
				if (diffDate < 0) {
					isError = true;
					record[cfgNippoFields.katsudobi.code].error = cfgNippoMessages.inputCheckHizuke;
					table[rowIndex].value[cfgNippoFields.shuryobi.code].error = cfgNippoMessages.inputCheckHizuke;
				}
				else if (diffDate == 0) {
					if (kaishiJikoku && shuryoJikoku) {
						// 時間比較
						if (sncLib.util.compareTime(kaishiJikoku, shuryoJikoku) < 0) {
							isError = true;
							table[rowIndex].value[cfgNippoFields.kaishijikoku.code].error = cfgNippoMessages.inputCheckJikoku;
							table[rowIndex].value[cfgNippoFields.shuryojikoku.code].error = cfgNippoMessages.inputCheckJikoku;
						}
					}
				}
			}
		}
		return isError;
	}

	/**
	 * スペースへラベルを表示
	 * @param {string} spaceFieldCode
	 * @param {string} labelName
	 */
	function setLabelOnSpace(spaceFieldCode, labelName) {
		// 営業活動レポートの情報がアプリ内に存在する場合、ラベルを表示
		var eigyoRepotoLabel = document.createElement('label');
		eigyoRepotoLabel.innerHTML = labelName;
		$(eigyoRepotoLabel).addClass('kintoneplugin-label');
		kintone.app.record.getSpaceElement(spaceFieldCode).appendChild(eigyoRepotoLabel);
	}

	/**
	 *
	 * レコード編集画面（新規、追加）の表示イベント
	 * レコード詳細画面、印刷画面の表示イベント
	 * 　フィールドの表示/非表示設定
	 * 　フィールドの入力可/否 を設定
	 *
	*/
	kintone.events.on([
		'app.record.create.show',
		'app.record.edit.show',
		'app.record.index.edit.show',
		'app.record.index.show',
		'app.record.detail.show',
		'app.record.print.show'
	], function (event) {
		var record = event.record;
		var loginUser = kintone.getLoginUser();
		// 詳細画面、印刷画面の場合
		if (event.type == 'app.record.detail.show' || event.type == 'app.record.print.show') {
			if (record[cfgNippoFields.eigyoRepoto.code].value.length == 0) {
				// 営業活動レポートの情報がアプリ内に存在しない場合、フィールドを非表示
				kintone.app.record.setFieldShown(cfgNippoFields.eigyoRepoto.code, false);
			} else {
				// 営業活動レポートの情報がアプリ内に存在する場合、ラベルを表示
				setLabelOnSpace(cfgNippoFields.eigyoRepotoSpace.code, cfgNippoFields.eigyoRepoto.name);
			}
		} else if (event.type == 'app.record.create.show' || event.type == 'app.record.edit.show') {
			// ログインユーザーを担当者検索コードへセット
			record[cfgNippoFields.tantoshaSearchId.code].value = loginUser.code;
			// ルックアップの値を取得
			record[cfgNippoFields.tantoshaSearchId.code].lookup = true;

			// 営業活動レポートの情報がアプリ内に存在する場合、ラベルを表示
			setLabelOnSpace(cfgNippoFields.eigyoRepotoSpace.code, cfgNippoFields.eigyoRepoto.name);
		}

		// ログインユーザーが管理アカウントではない場合
		if (config.kanriUsers.indexOf(loginUser.code) == -1) {
			// フィールドの表示/非表示設定
			sncLib.nok.util.setAppFieldsShown(cfgNippoFields);
			// レコード編集画面（新規、追加）の場合、
			// フィールドの入力可/否も設定する
			if (
				event.type == 'app.record.create.show'
				|| event.type == 'app.record.edit.show'
				|| event.type == 'app.record.index.edit.show'
			) {
				sncLib.nok.util.setAppFieldsDisabled(event.record, cfgNippoFields);
			}
		}
		return event;
	});

	/**
	 *
	 * レコード追加完了後、レコード編集完了後イベント
	 *   営業活動レポートアプリへの登録する情報の入力チェック
	 *
	*/
	kintone.events.on([
		'app.record.create.submit',
		'app.record.edit.submit',
		'app.record.index.edit.submit'
	], function (event) {
		var errorMessage = "";
		// 営業稼動レポートへの登録情報の入力チェック
		if (isErrorRequiredCheck(event, configEigyoRepotoLinkage.fields)) {
			errorMessage = cfgNippoMessages.requiredError;
		}
		// 入力チェック
		if (isErrorInputValue(event.record)) {
			errorMessage = (errorMessage) ? errorMessage + cfgNippoMessages.inputCheckError : cfgNippoMessages.inputCheckError;
		}

		if (errorMessage) {
			event.error = errorMessage;
		}
		return event;
	});

	/**
	 *
	 * レコード追加完了後、レコード編集完了後イベント
	 *   営業活動レポートアプリへの登録処理
	 *
	*/
	kintone.events.on([
		'app.record.create.submit.success',
		'app.record.edit.submit.success'
	], function (event) {
		var record = event.record;
		var postRecords;

		if (!sncLib.kintone.util.isBlankKintoneSubTable(record[cfgNippoFields.eigyoRepoto.code].value, configEigyoRepotoLinkage.fields)) {
			// 営業レポートアプリへの更新情報を作成
			postRecords = createEigyoRepotoData(record, record[cfgNippoFields.eigyoRepoto.code].value, configEigyoRepotoLinkage.fields);
		}

		var body = {};
		var payloadDetail = {};
		var recordDetail = {};

		// 日報アプリの更新情報を格納
		// サブテーブルの情報を削除する
		// record詳細
		recordDetail[cfgNippoFields.eigyoRepoto.code] = {};
		recordDetail[cfgNippoFields.eigyoRepoto.code]['value'] = [];
		// payload詳細
		payloadDetail['app'] = cfgNippo.app;
		payloadDetail['id'] = record[cfgNippoFields.recordId.code].value;
		payloadDetail['revision'] = record[cfgNippoFields.revision.code].value;
		payloadDetail['record'] = recordDetail;
		//body生成
		body['requests'] = [];
		body['requests'][0] = {
			'method': 'PUT',
			'api': '/k/v1/record.json',
			'payload': payloadDetail
		};

		// 営業レポートアプリの更新情報を格納
		if (postRecords) {
			payloadDetail = {};
			payloadDetail['app'] = config.eigyoRepoto.app;
			payloadDetail['records'] = postRecords;
			body['requests'][1] = {
				'method': 'POST',
				'api': '/k/v1/records.json',
				'payload': payloadDetail
			};
		}

		// REQUEST TOKENを設定
		body['__REQUEST_TOKEN__'] = kintone.getRequestToken();

		// 同期処理にて、更新を実施
		// 更新に失敗した場合、営業活動レポート情報は、日報アプリ側へ残す。
		return new kintone.Promise(function (resolve, reject) {
			kintone.api(kintone.api.url('/k/v1/bulkRequest', true), 'POST', body,
				function (res) {
					resolve(res);
				},
				function (err) {
					console.log(err);
					reject(err);
				}
			);
		}).then(function (resp) {
			return event;
		}).catch(function (error) {
			console.log(error);
			alert(cfgNippoMessages.postError);
			return event;
		});
	});

	/**
	 * フィールド変更イベント（新規、編集、一覧編集）
	 *   「開始時刻」が変更されたタイミング
	 *   「終了時刻」へ「開始時刻＋N分」の値をセット
	 * 　　　Nはconfig側のoffsetMinutesにて設定
	 *    ※関連レコード一覧の条件に文字列（自動計算）を指定している場合、
	 * 　　 return eventにより関連レコード一覧の内容がクリアされる。
	 * 　　 自動計算の変わりにJSにて自動計算を実装
	*/
	/*
	kintone.events.on([
		'app.record.create.change.' + cfgNippoFields.kaishijikoku.code,
		'app.record.edit.change.' + cfgNippoFields.kaishijikoku.code,
		'app.record.index.edit.change.' + cfgNippoFields.kaishijikoku.code
	], function (event) {
		//console.log(event);
		var row = event.changes.row;
		//console.log(row);
		var kaishi = row.value[cfgNippoFields.kaishijikoku.code].value;

		// 「開始時刻」の入力、数値チェック
		if (kaishi) {
			var min = sncLib.util.strToMin(kaishi) + cfgNippo.offsetMinutes;
			// 開始時刻+N分が24時を超えるかを設定
			if (min >= 24 * 60) {
				// 23時59分となるように調整
				min = 24 * 60 - 1;
			}
			var shuryo = sncLib.util.minToStr(min);
			//console.log(shuryo);
			row.value[cfgNippoFields.shuryojikoku.code].value = shuryo;
			return event;
		}
	});
	*/

	/**
	 * フィールド変更イベント（新規、編集、一覧編集）
	 *   「活動日」、「社員ID」が変更されたタイミング
	 *   「日報ID」へ「活動日＋社員ID」の値をセット
	*/
	kintone.events.on([
		'app.record.create.change.' + cfgNippoFields.katsudobi.code,
		'app.record.edit.change.' + cfgNippoFields.katsudobi.code,
		'app.record.index.edit.change.' + cfgNippoFields.katsudobi.code,
		'app.record.create.change.' + cfgNippoFields.tantoshaId.code,
		'app.record.edit.change.' + cfgNippoFields.tantoshaId.code,
		'app.record.index.edit.change.' + cfgNippoFields.tantoshaId.code
	], function (event) {
		var record = event.record;
		var katsudobi = record[cfgNippoFields.katsudobi.code].value;
		var tantoshaId = record[cfgNippoFields.tantoshaId.code].value;
		var nippoId = '';
		if (katsudobi && tantoshaId) {
			nippoId = sncLib.nok.data.createNippoId(katsudobi, tantoshaId);
		}
		record[cfgNippoFields.nippoId.code].value = nippoId;
		return event;
	});

})(jQuery, window.nokConfig, window.eigyoRepotoLinkageConfig, window.snc);
