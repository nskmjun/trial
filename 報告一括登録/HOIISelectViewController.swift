//
//  HOIISelectViewController.swift
//  cfm_catalogue
//
//  Created by Shingo Matsuura on 2019/02/25.
//  Copyright © 2019 Shingo Matsuura. All rights reserved.
//

import UIKit

class HOIISelectViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // Ver.ボタン押下
    @IBAction func didTouchVersionButton(_ sender: UIBarButtonItem)
    {
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        
        showAlert(title: "バージョン情報", message: "Version: \(version) \n Build: \(build)", inViewController: self)
    }
    // H&Oボタン押下
    @IBAction func didTouchHOButton(_ sender: UIButton)
    {
        UserDefaults.standard.set(kBelong_HO, forKey: kUserDefaults_Key_Belong)
        UserDefaults.standard.synchronize()
    }
    
    // I&Iボタン押下
    @IBAction func didTouchIIButton(_ sender: UIButton)
    {
        UserDefaults.standard.set(kBelong_II, forKey: kUserDefaults_Key_Belong)
        UserDefaults.standard.synchronize()
    }
}

