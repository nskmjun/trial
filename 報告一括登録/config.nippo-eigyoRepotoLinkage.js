/**
 * @fileoverview  一括報告アプリ　営業レポート連携設定情報
 * @author SNC
 * @version 2.5.0
 * @KKH_version 1.0.0
 */
(function (config) {
	const cfgNippoFields = config.nippo.fields;
	const cfgEigyoRepotoFields = config.eigyoRepoto.fields;

	// グローバル変数 更新対象情報
	window.eigyoRepotoLinkageConfig = window.eigyoRepotoLinkageConfig || {
		fields: {
			header: [
				// 活動日
				{
					source: {
						code: cfgNippoFields.katsudobi.code,
						required: true
					},
					target: {
						type: 'DATE',
						code: cfgEigyoRepotoFields.katsudobi.code
					}
				},
				// 担当者検索コード
				{
					source: {
						code: cfgNippoFields.tantoshaSearchId.code,
						required: true
					},
					target: {
						type: 'SINGLE_LINE_TEXT',
						code: cfgEigyoRepotoFields.tantoshaSearchId.code
					}
				},
				/*
				// 予定・実績
				{
					source: {
						code: cfgNippoFields.yoteiJisseki.code,
						required: true
					},
					target: {
						type: 'RADIO_BUTTON',
						code: cfgEigyoRepotoFields.yoteiJisseki.code
					}
				},
				*/
				// 日報ID
				{
					source: {
						code: cfgNippoFields.nippoId.code,
						required: true
					},
					target: {
						type: 'SINGLE_LINE_TEXT',
						code: cfgEigyoRepotoFields.nippoId.code
					}
				}
			],
			detail: [
				// 予定・実績
				{
					source: {
						code: cfgNippoFields.yoteiJisseki.code,
						required: true
					},
					target: {
						type: 'RADIO_BUTTON',
						code: cfgEigyoRepotoFields.yoteiJisseki.code
					}
				},
				// 開始時刻
				{
					source: {
						code: cfgNippoFields.kaishijikoku.code,
						required: true
					},
					target: {
						type: 'DATETIME',
						code: cfgEigyoRepotoFields.kaishijikoku.code
					}
				},
				// 終了日
				{
					source: {
						code: cfgNippoFields.shuryobi.code,
						required: true
					},
					target: {
						type: 'DATE',
						code: cfgEigyoRepotoFields.shuryobi.code
					}
				}
				,
				// 終了時刻
				{
					source: {
						code: cfgNippoFields.shuryojikoku.code,
						required: true
					},
					target: {
						type: 'DATETIME',
						code: cfgEigyoRepotoFields.shuryojikoku.code
					}
				}
				,
				// 活動種別
				{
					source: {
						code: cfgNippoFields.katsudoshubetsu.code,
						required: true
					},
					target: {
						type: 'SINGLE_LINE_TEXT',
						code: cfgEigyoRepotoFields.katsudoshubetsu.code
					}
				}
				,
				// 顧客名検索コード
				{
					source: {
						code: cfgNippoFields.kokyakuSearchId.code,
						required: true
					},
					target: {
						type: 'SINGLE_LINE_TEXT',
						code: cfgEigyoRepotoFields.kokyakuSearchId.code
					}
				},
				// 訪問目的
				{
					source: {
						code: cfgNippoFields.homonMokuteki.code,
						required: false
					},
					target: {
						type: 'DROP_DOWN',
						code: cfgEigyoRepotoFields.homonMokuteki.code
					}
				},
				// 訪問結果
				{
					source: {
						code: cfgNippoFields.homongoKekka.code,
						required: false
					},
					target: {
						type: 'DROP_DOWN',
						code: cfgEigyoRepotoFields.homongoKekka.code
					}
				},
				// 備考
				{
					source: {
						code: cfgNippoFields.biko.code,
						required: false
					},
					target: {
						type: 'SINGLE_LINE_TEXT',
						code: cfgEigyoRepotoFields.biko.code
					}
				}
			]
		}
	};
})(window.nokConfig);
